package it.unibo.oop.lab.lambda.ex02;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        ArrayList<String> list = new ArrayList<String>();
        songs.forEach(e -> list.add(e.songName));
        return list.stream().sorted();
    }

    @Override
    public Stream<String> albumNames() {
        ArrayList<String> list = new ArrayList<String>();
        albums.forEach((k, v) -> list.add(k));
        return list.stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
        ArrayList<String> list = new ArrayList<String>();
        albums.forEach((k, v) -> list.add(k));
        return list
               .stream()
               .filter(k -> albums.get(k).equals(year)); //Filtering here, instead of within the forEach
    }

    @Override
    public int countSongs(final String albumName) {
        return (int) songs
                     .stream()
                     .filter(e -> e.getAlbumName().equals(albumName))
                     .count();
    }

    @Override
    public int countSongsInNoAlbum() {
        return (int) songs
                     .stream()
                     .filter(e -> !e.getAlbumName().isPresent()) //TODO reused code, maybe delegate to function that returns streams, based on consumer
                     .count();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        Stream<Song> filtered = songs
                                .stream()
                                .filter(e -> e.getAlbumName().equals(albumName));
        double count = filtered.count();
        if (count == 0) {
            return OptionalDouble.of(count);  //Should it return a null value instead?
            } else {
            return OptionalDouble.of(filtered
                                     .mapToDouble(e -> e.getDuration())
                                     .sum() 
                                     / count); 
            }
    }

    @Override
    public Optional<String> longestSong() {
        return Optional.of(
                          songs
                          .stream()
                          .max(new Comparator<Song>() {
                                 public int compare(final Song s1, final Song s2) {
                                 return (int) (s1.duration - s2.duration);
                                 }
                          })
                          .get()
                          .getSongName());
    }


    @Override
    public Optional<String> longestAlbum() {
       /* Map<String, Set<Double>> map = new HashMap<String, Set<Double>>();
        songs.forEach(e -> {
            HashSet<Double> set = new HashSet<Double>();
            set.add(e.getDuration());
            map.merge(e.getAlbumName().orElse("None"), set, (s1, s2) -> {
                s1.addAll(s2);
                return s1;
            });
        });
        */
        Map<String, Set<Song>> map = mapAlbums();
        map.remove("None");
        Double max = null;
        String longest = null;

        for (Map.Entry<String, Set<Song>> entry : map.entrySet()) {
            Double sum = entry.getValue().stream().mapToDouble(e -> e.getDuration()).sum();
            if (max == null || sum.compareTo(max) > 0) {
                max = sum;
                longest = entry.getKey();
            }
        }
        return Optional.of(longest);
    }

    /**
     * @return returns a Map instance wherein each Album name is a key and its value is the relative Set of Songs
     */
    public Map<String, Set<Song>> mapAlbums() {
        Map<String, Set<Song>> map = new HashMap<String, Set<Song>>();
        songs.forEach(e -> {
            HashSet<Song> set = new HashSet<Song>();
            set.add(e);
            map.merge(e.getAlbumName().orElse("None"), set, (s1, s2) -> {
                s1.addAll(s2);
                return s1;
            });
        });
       return map; 
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
